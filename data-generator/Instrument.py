
class Instrument:

    def __init__(self, name, basePrice, drift, variance):
        self.name = name
        self.basePrice = basePrice
        self.drift = drift
        self.variance = variance

    def calculateNextPrice(self, type):
        # todo - make this actually consider drift, variance, type and time
        print("type: {}".format(type))
        return self.basePrice

    def tidy(self):
        return "Name: {}; BasePrice: {}; Drift: {}; Variance: {}".format(self.name, self.basePrice, self.drift, self.variance)
